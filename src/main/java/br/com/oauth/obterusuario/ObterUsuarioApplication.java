package br.com.oauth.obterusuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObterUsuarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ObterUsuarioApplication.class, args);
	}

}
