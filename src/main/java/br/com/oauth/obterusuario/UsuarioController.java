package br.com.oauth.obterusuario;

import br.com.oauth.obterusuario.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @PostMapping
    public Usuario obterUsuario(@AuthenticationPrincipal Usuario usuario) {
        return usuario;
    }
}
